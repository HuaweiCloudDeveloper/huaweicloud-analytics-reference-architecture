# DataX采集日志数据-对接obs对象存储系统

## 分析与运营平台之DataX对接OBS

DataX是一个开源的数据同步框架，实现包括关系型数据库\(MySQL、Oracle等\)、PostgreSQL、HDFS、Hive、HBase、FTP等各种异构数据源之间稳定、高效的数据同步功能。OBS可以替代Hadoop系统中的HDFS服务，为大数据计算提供“数据湖”存储。

## DataX安装

**DataX下载安装**

请在官网进行下载：[DataX](https://github.com/alibaba/DataX)

**编译DataX**

1.  为了更好的使DataX对接obs，需要将DataX的hdfsreader和hdfswriter模块依赖的hadoop版本从_2.7.1升到_2.8.3，修改“datax\\hdfswriter\\pom.xml”和“datax\\hdfsreader\\pom.xml”文件配置：

    ```
    <properties>
    <hadoop.version>2.8.3</hadoop.version>
    </properties>
    ```

2.  执行如下命令：

    ```
    mvn -U clean package assembly:assembly -Dmaven.test.skip=true
    ```

3.  在datax源码根目录“/target”目录下生成datax.tar.gz压缩文件。

**DataX对接OBS**

在Github下载hadoop-huaweicloud对应版本的jar包，将上述下载的jar包放入“/opt/module/datax/plugin/writer/hdfswriter/libs”和“/opt/module/datax/plugin/reader/hdfsreader/libs”目录下面，实现DataX和OBS的对接。

hadoop-huaweicloud下载地址：[obsa-hdfs](https://github.com/huaweicloud/obsa-hdfs)

## 分析与运营平台之业务数据采集DataX配置

1.  在$datax\_home下面新建配置文件user2obs.json。

    ![](img/zh-cn_image_0000001263387278.png)

    ![](img/zh-cn_image_0000001310947349.png)

2.  执行如下命令，将mysql数据库user表的数据每天定时同步到obs对象存储系统“/origin\_data/db”目录下面。

    ```
    python /opt/module/datax/bin/datax.py /opt/module/datax/job/user2obs.json
    ```

    ![](img/zh-cn_image_0000001263067466.png)


