package com.huaweicloud.flume;

import org.apache.flume.Context;
import org.apache.flume.Event;
import org.apache.flume.interceptor.Interceptor;

import java.nio.charset.StandardCharsets;
import java.util.Iterator;
import java.util.List;

public class ETLInterceptor implements Interceptor {

    @Override
    public void initialize() {

    }

    //拦截器引用才会生效

       /* a1.sources.r1.interceptors =  i1
    a1.sources.r1.interceptors.i1.type = com.atguigu.flume.interceptor.ETLInterceptor$Builder*/

/*    数据进hive之前 必须先对数据做一个校验   必须是标准的json格式
            用flume的拦截器做一个清洗*/
    @Override
    public Event intercept(Event event) {

        byte[] body = event.getBody();
        String log = new String(body, StandardCharsets.UTF_8);

        if (JSONUtils.isJSONValidate(log)) {
            return event;
        } else {
            return null;
        }
    }

//    @Override
    public List<Event> intercept(List<Event> list) {

        Iterator<Event> iterator = list.iterator();


        //source 里面 有的有batchsize  支持批量处理的
//source 里面 有的没有batchsize 不支持批量处理的

        while (iterator.hasNext()){
            Event next = iterator.next();
            if(intercept(next)==null){
                iterator.remove();
                //移除集合中的元素

            }
        }



        list.removeIf(event ->intercept(event)==null );
        //函数式编程
        //移除为true的元素


        return list;
    }

   /* a1.sources.r1.interceptors =  i1
    a1.sources.r1.interceptors.i1.type = com.atguigu.flume.interceptor.ETLInterceptor$Builder*/
    public static class Builder implements Interceptor.Builder{

        @Override
        public Interceptor build() {
            return new ETLInterceptor();
        }
        @Override
        public void configure(Context context) {
            //Context     flume运行的上下文
            //context能获取flume配置文件中给拦截器传递的参数
            //
            /*context.getString("aaa","defaultvalue");*/
        }

    }

    @Override
    public void close() {

    }
}
