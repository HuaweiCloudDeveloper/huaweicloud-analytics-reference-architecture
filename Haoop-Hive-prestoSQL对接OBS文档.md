# Haoop-Hive-prestoSQL对接OBS文档

## PrestoSQL对接OBS简介

Presto分为prestoSql（现更名为Trino）和PrestoDB两大分支或是发行版。

Presto on OBS仅支持prestoSql/Trino发行版，下述对接步骤以prestoSql-333版本为例。

**概述**

hadoop系统提供了分布式存储，计算和资源调度引擎，用于大规模数据处理和分析。OBS服务实现了hadoop的HDFS协议，在大数据场景中可以替代hadoop系统中的HDFS服务，实现Spark、MapReduce、Hive、HBase等大数据生态与OBS服务的对接，为大数据计算提供“数据湖”存储。

注：HDFS协议：hadoop中定义了HDFS协议（通过FileSystem抽象类），其他各类存储系统均可以实现HDFS协议，例如hadoop中内置的HDFS服务，华为云的OBS对象存储服务

**数据存储在OBS和HDFS有什么区别**

OBS（Object Storage Service）即对象存储服务，是一个基于对象的海量存储服务，为客户提供海量、安全、高可靠、低成本的数据存储能力。数据存储和计算分离，使用时无需考虑容量限制，并且提供多种存储类型供选择，满足客户各类业务场景诉求。

HDFS是Hadoop分布式文件系统，数据存储和计算不分离，集群成本较高，自建存储服务器的数据存储受限于搭建服务器时，使用的硬件设备如果存储量不够，需要重新购买存储服务器，进行人工扩容。

**应用场景**

-   应用场景一：华为云服务器大数据服务MRS、DLI已在版本中包含OBSA-HDFS插件，无需下载安装，即可使用OBS作为大数据存储。
-   应用场景二：华为云租户基于IAAS资源，自建大数据计算平台，可下载并配置OBSA-HDFS插件，使用OBS作为大数据存储。

## 安装Hadoop

1.  下载hadoop-3.1.1.tar.gz。
    1.  解压到/opt/module/hadoop-3.1.1目录。
    2.  在/etc/profile文件中增加如下配置。

        ```
        export HADOOP_HOME=/opt/module/hadoop-3.1.1
        export PATH=$HADOOP_HOME/bin:$HADOOP_HOME/sbin:$PATH
        ```

2.  安装hadoop-huaweicloud。
    1.  在官方Github下载[hadoop-huaweicloud](https://github.com/huaweicloud/obsa-hdfs)。

        注：若没有匹配版本的jar包，可自行修改hadoop-huaweicloud目录下pom文件中的hadoop版本重新编译生成。

    2.  将hadoop-huaweicloud-x.x.x-hw-y.jar拷贝到“/opt/module/hadoop-3.1.1/share/hadoop/tools/lib和/opt/module/hadoop-3.1.1/share/hadoop/common/lib”目录下。

        注：hadoop-huaweicloud-$\{hadoopversion\}-hw-$\{version\}.jar版本规则：hadoopversion为对应的hadoop版本号，version为hadoop-huaweicloud版本号。

3.  修改core-site.xml。

    Hadoop的版本为3.1.1，按照[《对象存储服务OBSA-HDFS使用指南》](https://github.com/huaweicloud/obsa-hdfs/tree/master/release/doc)配置hadoop修改core-site.xml配置文件。

    ![](img/zh-cn_image_0000001263557762.jpg)

4.  获取AK/SK，请参考[获取AK/SK](https://support.huaweicloud.com/devg-apisign/api-sign-provide-aksk.html)。

    ![](img/zh-cn_image_0000001263078130.jpg)

5.  在对象存储服务OBS中创建桶。

    ![](img/zh-cn_image_0000001310838421.jpg)


**示例**

1.  执行如下命令。

    ```
    hadoop fs -ls obs://obs-east-bkt001/
    ```

    ![](img/zh-cn_image_0000001310839189.png)

2.  MR程序，本地编辑文件test，上传到“obs://obs-east-bkt001/input”路径下。

    ![](img/zh-cn_image_0000001310959093.jpg)

3.  执行如下命令，执行MR。

    ```
    hadoop jar $HADOOP_HOME/share/hadoop/mapreduce/hadoop-mapreduce-examples-3.1.1.jar wordcount obs://obs-east-bkt001/input/test.txt obs://obs-east-bkt001/output
    ```

4.  执行如下命令，查看结果。

    ```
    hadoop fs -cat  obs://obs-east-bkt001/output/part-r-00000
    ```

    ![](img/zh-cn_image_0000001263079410.png)


## 安装Hive

Hive的版本为3.1.1。

1.  下载安装hive-3.1.1，修改hive-site.xml配置文件。

    ![](img/zh-cn_image_0000001263080786.png)

2.  创建student表，插入记录。

    ```
    hive>
    create table student(id int comment "student id",name string comment "student name",age int comment "student age")
    comment "student info"
    row format delimited fields terminated by ",";
    hive>
    insert into table student select 6,"yangdong",29;
    insert into table student select 7,"sunlei",30;
    ```

3.  验证是否对接成功。

    示例如下，示例中的location为“obs://obs-east-bkt001/warehouse/hive/student”，查询结果显示hive对接obs成功。

    ![](img/zh-cn_image_0000001263560926.jpg)

    ![](img/zh-cn_image_0000001263400986.jpg)


## **Presto对接OBS架构流程图**

![](img/zh-cn_image_0000001311169773.png)

执行流程：客户端prestocli通过http请求发送sql语句给presto coordinator，coordinator对sql语句进行解析生成执行计划，根据生成的执行计划生成stage和task，coordinator将每一个stage的task任务分发到worker上进行执行，worker通过connector从数据源中读取数据，每个Stage的task在worker内存中进行计算，coordinator将执行结果返回给客户端prestocli显示输出。

## 安装presto server

**下载安装prestoSQL-333**

1.  下载Presto客户端和服务端：
    -   客户端：[presto-cli](https://repo1.maven.org/maven2/io/prestosql/presto-cli/)
    -   服务端：[presto-server](https://repo1.maven.org/maven2/io/prestosql/presto-server)

2.  下载[华为OBS-HDFS插件](https://github.com/huaweicloud/obsa-hdfs/tree/master/release)。
3.  解压Presto服务端。

    ```
    tar –zxvf presto-server-333.tar.gz
    ```

4.  在presto根目录“/plugin/hive-hadoop2”下放入如下两个jar包。
    -   [hadoop-huaweicloud-2.8.3-hw-36.jar](https://github.com/huaweicloud/obsa-hdfs/blob/master/release/hadoop-huaweicloud-2.8.3-hw-36.jar)
    -   Apache commons-lang-xxx.jar（可从maven中央仓库下载或从hadoop目录中拷贝）


**配置presto**

在安装目录里创建etc目录，这目录会有以下配置（自己创建）：

-   结点配置文件：每个结点的环境配置
-   JVM配置文件：Java虚拟机的命令行选项
-   Server配置文件（Config Properties）：Persto server的配置
-   Catelog配置文件：配置presto的各种Connector（数据源）
-   日志配置文件：配置Presto日志

1.  结点配置文件。

    结点属性文件“etc/node.properties”，包含每个结点的配置。一个结点是一个Presto实例。这文件一般是在Presto第一次安装时创建的。以下是最小配置：

    ![](img/zh-cn_image_0000001310964997.jpg)

    参数说明：

    -   node.environment：环境名字，Presto集群中的结点的环境名字都必须是一样的。
    -   node.id：唯一标识，每个结点的标识都必须是唯一的。就算重启或升级Presto都必须还保持原来的标识。
    -   node.data-dir：数据目录，Presto用它来保存log和其他数据。

    注：data目录需要自己手动创建。

2.  JVM配置文件。

    JVM配置文件etc/jvm.config，包含启动Java虚拟机时的命令行选项。格式是每一行是一个命令行选项。此文件数据是由shell解析，所以选项中包含空格或特殊字符会被忽略。

    ![](img/zh-cn_image_0000001263405046.jpg)

    注：以上参数都是官网参数，实际环境需要调整。

3.  Server配置文件。

    配置属性文件etc/config.properties，包含Presto server的配置。Presto server可以同时为coordinator和worker，但一个大集群里最好就是只指定一台机器为coordinator。

    1.  coordinator节点的配置文件。

        ![](img/zh-cn_image_0000001310965397.jpg)

    2.  worker节点的配置文件。

        ![](img/zh-cn_image_0000001263085610.jpg)

    参数说明：

    -   coordinator：是否运行该实例为coordinator（接受client的查询和管理查询执行）。node-scheduler.include-coordinator:coordinator是否也作为work。对于大型集群来说，在coordinator里做worker的工作会影响查询性能。 http-server.http.port:指定HTTP端口。Presto使用HTTP来与外部和内部进行交流。
    -   query.max-memory：查询能用到的最大总内存。
    -   query.max-memory-per-node：查询能用到的最大单结点内存。
    -   discovery-server.enabled: Presto使用Discovery服务去找到集群中的所有结点。每个Presto实例在启动时都会在Discovery服务里注册。这样可以简化部署，不需要额外的服务，Presto的coordinator内置一个Discovery服务。也是使用HTTP端口。
    -   discovery.uri：Discovery服务的URI。将example.net:8080替换为coordinator的host和端口。这个URI不能以斜杠结尾，这个错误需特别注意，不然会报404错误。
    -   jmx.rmiregistry.port：指定JMX RMI的注册。JMX client可以连接此端口。
    -   jmx.rmiserver.port：指定JXM RMI的服务器。可通过JMX监听。

4.  Catelog配置文件（重点）。

    hive connector配置如下:

    1.  在etc目录下创建catalog目录。
    2.  创建一个hive connector的配置文件：hive.properties。

        ![](img/zh-cn_image_0000001310852541.png)

        参数说明：

        -   connector.name：连接名。
        -   hive.metastore.uri：配置hive metastore连接。
        -   hive.config.resources：指定hadoop的配置文件，注意core-site.xml需要按照[《对象存储服务OBSA-HDFS使用指南》](https://github.com/huaweicloud/obsa-hdfs/tree/master/release/doc)配置。
        -   hive.allow-drop-table：给删表权限。

5.  日志配置文件。

    创建文件log.properties，写入内容：com.facebook.presto=INFO

    备注：日志级别有四种，DEBUG, INFO, WARN和ERROR。


## 启动presto

1.  执行如下命令，启动hive metastore。

    ```
    hive --service metastore &
    ```

2.  执行如下命令，启动presto server。

    ```
    bin/launcher start
    ```

3.  将presto-cli-333-executable.jar命名为presto，放在bin目录下，然后执行如下命令，赋予执行权限。

    ```
    chmod +x prestocli
    ```

4.  执行如下命令，启动client。

    ```
    ./prestocli --server XX.XX.XX.XX:8881 --catalog hive --schema default
    ```

    注意：hadoop3.1.1版本需要jdk1.8环境，presto333版本需要jdk11环境。需要在presto启动文件launcher里加上为presto333指定jdk11运行环境。


## 关闭presto服务

执行如下命令，关闭presto服务。

```
bin/launcher stop
```

## presto查询hive表

执行如下命令，查询hive表。

```
./presto --server XX.XX.XX.XX:8881 --catalog hive --schema default
```

![](img/zh-cn_image_0000001311167525.png)

结果显示presto对接OBS成功。

