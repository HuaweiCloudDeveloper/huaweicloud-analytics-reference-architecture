#!/bin/bash

if [ -n "$2" ] ;then
    do_date=$2
else 
    do_date=`date -d "-1 day" +%F`
fi


dws_user_action_daycount="
set hive.exec.dynamic.partition.mode=nonstrict;
with
tmp_login as
(
select
 substr(timestr,0,10) dt,
 tenantdomain,
 uid,
 count(*) login_count
from dwd_page_log
where uid is not null
and last_page_id ='login'
group by substr(timestr,0,10),tenantdomain,uid
),
tmp_action as
(
select
substr(timestr,0,10) dt,
tenantdomain,
uid,
sum(if(action_id='firstPage',1,0)) firstPage,
sum(if(action_id='myOrders',1,0)) myOrders,
sum(if(action_id='seveiceList',1,0)) seveiceList
from dwd_action_log
where uid is not null
and action_id in ('firstPage','myOrders','seveiceList')
group by substr(timestr,0,10),tenantdomain, uid
),
tmp_order as
(
select
domain,
date_format(created_time,'yyyy-MM-dd') dt,
uid,
count(*) order_count,
sum(original_amount) order_original_amount,
sum(final_amount) order_final_amount
from dwd_t_order
group by date_format(created_time,'yyyy-MM-dd'),domain,uid
)
insert overwrite table dws_user_action_daycount partition(dt)
select
coalesce(tmp_login.tenantdomain,tmp_action.tenantdomain,tmp_order.domain) domain,
coalesce(tmp_login.uid,tmp_action.uid,tmp_order.uid),
nvl(login_count,0),
nvl(firstPage,0),
nvl(myOrders,0),
nvl(seveiceList,0),
nvl(order_count,0),
nvl(order_original_amount,0),
nvl(order_final_amount,0),
coalesce(tmp_login.dt,tmp_action.dt,tmp_order.dt)
from tmp_login
full outer join tmp_action
on tmp_login.uid=tmp_action.uid
and tmp_login.dt=tmp_action.dt
and tmp_login.tenantdomain=tmp_action.tenantdomain
full outer join tmp_order
on coalesce(tmp_login.uid,tmp_action.uid)=tmp_order.uid
and coalesce(tmp_login.dt,tmp_action.dt)=tmp_order.dt
and coalesce(tmp_login.tenantdomain,tmp_action.tenantdomain)=tmp_order.domain;
"



dws_sku_action_daycount="
set hive.exec.dynamic.partition.mode=nonstrict;
with
tmp_order as
(
select
domain,
date_format(created_time,'yyyy-MM-dd') dt,
sku_id,
count(*) order_count,
sum(amount) sku_num,
sum(original_amount) order_original_amount,
sum(final_amount) order_final_amount
from dwd_t_order
group by date_format(created_time,'yyyy-MM-dd'),domain,sku_id
)
insert overwrite table dws_sku_action_daycount partition(dt)
select
domain,
sku_id,
order_count,
sku_num,
order_original_amount,
order_final_amount,
dt
from tmp_order;
"


case $1 in
"dws_user_action_daycount" )
hive -e "$dws_user_action_daycount"
;;
"dws_sku_action_daycount" )
hive -e "$dws_sku_action_daycount"
;;
"all" )
hive -e "$dws_user_action_daycount$dws_sku_action_daycount"
;;
esac
