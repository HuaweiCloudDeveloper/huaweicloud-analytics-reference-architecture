#!/bin/bash

if [ -n "$2" ] ;then
do_date=$2
else 
do_date=`date -d "-1 day" +%F`
fi
dim_sku_info="
with sku as
(
 select
id,
service_id,
price,
created_by,
created_time,
updated_by,
updated_time,
revision,
delete_flag,
domain
from ods_t_service_sku
where dt = '$do_date'
),
service as
(
select 
id,
service_name,
service_desc,
servie_status,
display_price,
domain
from ods_t_housekeeper_service
where dt = '$do_date'
)
 
insert
overwrite
table
dim_sku_info
partition
(
dt='$do_date'
)
select
sku.domain,
sku.id,
sku.service_id,
sku.price,
sku.created_by,
sku.created_time,
sku.updated_by,
sku.updated_time,
sku.revision,
sku.delete_flag,
service.service_name,
service.service_desc,
service.servie_status,
service.display_price
from sku
left join service on sku.service_id = service.id
and sku.domain = service.domain;
"


dim_sku_info="insert overwrite table dim_user partition(dt='$do_date')
select
domain,
id,
user_id,
md5(user_name),
user_role,
default_address,
md5(phone_no),
created_by,
created_time,
updated_by,
updated_time,
md5(email),
md5(id_card),
account_type,
delete_flag
from ods_user
where dt = '$do_date' ;
"


case $1 in
"dim_user"){
    hive -e "$dim_user"
};;
"dim_sku_info"){
    hive -e "$dim_sku_info"
};;
"all"){
    hive -e "$dim_user$dim_sku_info"
};;
esac
