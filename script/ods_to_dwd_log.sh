#!/bin/bash
if [ -n "$2" ] ;then
do_date=$2
else
do_date=`date -d "-1 day" +%F`
fi
dwd_page_log="
insert overwrite table dwd_page_log partition(dt='$do_date')
select
get_json_object(line,'$.u_ud'),
get_json_object(line,'$.userId'),
get_json_object(line,'$.tenantDomain'),
get_json_object(line,'$.is_new'),
get_json_object(line,'$.b_iev'),
get_json_object(line,'$.msg.information.duringTime'),
get_json_object(line,'$.msg.information.item'),
get_json_object(line,'$.msg.information.item_type'),
get_json_object(line,'$.msg.information.lastPageId'),
get_json_object(line,'$.msg.information.pageId'),
get_json_object(line,'$.msg.information.serviceId'),
get_json_object(line,'$.msg.information.source_type'),
get_json_object(line,'$.msg.ts'),
get_json_object(line,'$.time')
from ods_log
where dt='$do_date'
and get_json_object(line,'$.msg.information') is not null;
"
dwd_action_log="
insert overwrite table dwd_action_log partition(dt='$do_date')
select
get_json_object(line,'$.u_ud'),
get_json_object(line,'$.userId'),
get_json_object(line,'$.tenantDomain'),
get_json_object(line,'$.is_new'),
get_json_object(line,'$.b_iev'),
get_json_object(line,'$.msg.information.duringTime'),
get_json_object(line,'$.msg.information.item'),
get_json_object(line,'$.msg.information.item_type'),
get_json_object(line,'$.msg.information.lastPageId'),
get_json_object(line,'$.msg.information.pageId'),
get_json_object(line,'$.msg.information.serviceId'),
get_json_object(line,'$.msg.information.source_type'),
get_json_object(action,'$.item'),
get_json_object(action,'$.item_type'),
get_json_object(line,'$.msg.ts'),
get_json_object(line,'$.time')
from ods_log lateral view explode_json_array(get_json_object(line,'$.msg.actions')) tmp as action
where dt='$do_date'
and get_json_object(line,'$.msg.actions') is not null;"
case $1 in
 dwd_page_log )
hive -e "$dwd_page_log"
;;
dwd_action_log )
hive -e "$dwd_action_log"
;;
all )
 hive -e "$dwd_page_log$dwd_action_log"
;;
esac
