#!/bin/bash
obs_path='obs://obs-east-bkt001/origin_data/log'
hive=/opt/module/hive/bin/hive
hadoop=/opt/module/hadoop-3.1.1/bin/hadoop
# 如果是输入的日期按照取输入日期；如果没输入日期取当前时间的前一天
if [ -n "$1" ] ;then
do_date=$1
else 
do_date=`date -d "-1 day" +%F`
fi 

handle_targetdir() {
obs_target="${obs_path}/${do_date}"
hadoop fs -test -e $obs_target
if [[ $? -eq 1 ]]; then
echo "路径$1不存在，正在创建"
hadoop fs -mkdir -p "$obs_target"
else
echo "路径$1已经存在"
fs_count=$(hadoop fs -count $obs_target)
content_size=$(echo $fs_count | awk '{print $3}')	
if [[ $content_size -eq 0 ]]; then
echo "路径$obs_target为空"
else
echo "路径$obs_target不为空，正在清空......"
hadoop fs -rm -r -f $obs_target/*
fi
 fi
}

handle_targetdir
###
hadoop fs -cp obs://obs-east-bkt001/1.log   obs://obs-east-bkt001/origin_data/log/$do_date/1.log

sql="
load data inpath '${obs_path}/${do_date}' into table ods_log partition(dt='$do_date');
"
$hive -e "$sql"
