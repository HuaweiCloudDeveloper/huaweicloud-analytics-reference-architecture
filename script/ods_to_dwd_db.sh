#!/bin/bash
if [ -n "$2" ] ;then
do_date=$2
else 
do_date=`date -d "-1 day" +%F`
fi
dwd_order_info="insert overwrite table dwd_t_order partition(dt='$do_date')
select
domain,
id,
order_number,
customer_id,
customer_name,
customer_phone,
sku_id ,  
appointment_time,
remark,
service_detail,
address,
status,
employee_id,
employee_name,
employee_phone,
created_time,
updated_time,
price,
amount,
payment,
payment
from ods_t_order
where dt = '$do_date';
"

case $1 in
dwd_order_info )
hive -e "$dwd_order_info"
;;
all )
hive -e "$dwd_order_info"
;;
esac
