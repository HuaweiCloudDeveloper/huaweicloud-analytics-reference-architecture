# flume采集业务数据-对接obs对象存储系统

## 分析与运营平台之Flume对接OBS

Flume是一个分布式的、可靠的和高可用的大数据日志采集框架。

Flume对接OBS文档，请参考：[Flume对接OBS](https://support.huaweicloud.com/bestpractice-obs/obs_05_1512.html)

## Flume安装

Flume下载安装部署请参考官网：[http://flume.apache.org/](http://flume.apache.org/)

## 分析与运营平台之采集日志Flume配置

本分析与运营平台Flume采用双层flume结构，用于分摊日志服务器的压力。

**采集日志Flume**

1.  在$FLUME\_HOM目录下新建“jobs/taildir\_kafka.conf”配置文件。

    ![](img/zh-cn_image_0000001310956613.png)

2.  采集日志Flume的source组件采用taildir，能够采集多目录的日志文件，断点续传，记录每个日志文件最新的消费位置，agent进程重启后不会有重复消费的问题。Flume的source用flume的拦截器做一个简单的数据清洗，数据进hive之前，先对数据做一个校验。数据格式为标准的json格式。

    ![](img/zh-cn_image_0000001311036673.png)


**消费Kafka数据Flume**

1.  在$FLUME\_HOM目录下新建“jobs/kafka\_obs.conf”配置文件。

    Flume将kafka中对应topic的数据写往OBS对象存储系统，每天的产生的用户行为日志按天分区存储。

    设置参数hdfs.rollInterval=3600，hdfs.rollSize=134217728，文件达到128M时或者文件新建时间超过一天，滚动生成一个新文件；过多的小文件，需要更多的元数据存储空间和启动更多map计算任务，影响性能；为了减少对存储空间的占用，减少网络IO，加快计算速度，选择gzip压缩格式，gzip压缩比较高，有hadoop native库，直接处理文本一样，但是不支持切片，可以在flume日志采集的时候限制日志文件大小。

    ![](img/zh-cn_image_0000001310836589.png)

2.  每天的日志数据存到一个路径hive进行批处理的时候，只要拿到对应天的数据，默认hdfs.useLocalTimeStamp设置false不使用服务器本地时间要求flume发送的Event事件的header中带有key为 timestamp的时间戳，使用该时间用于替换Flume配置文件中的逃逸字符；编写flume的timestamp拦截器，获取日志的时间，设置Event的timestamp时间戳，该value值决定日志写到obs对象存储系统哪个路径下面。

    ![](img/zh-cn_image_0000001263396554.png)

3.  日志文件按天分区，在obs对象存储系统“/origindata/log”目录下，存储每天的日志文件。

    ![](img/zh-cn_image_0000001263556490.png)


