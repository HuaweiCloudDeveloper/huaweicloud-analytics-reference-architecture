

# **项目介绍**

本项目是是华为云开发者团队基于SaaS项目技术支持实践，通过前端埋点产生的日志行为数据以及后端产生的业务数据为基准，通过在hive数仓中进行离线分析，按天分区存储，将结果存储在obs中，最后通过dlv进行可视化展示。

![](img/可视化.PNG)

## 数据来源

大数据解决方案有一个或者多个数据源对接，包括：

- 关系型数据库（Relational Database Service，简称RDS）业务数据。
- 应用程序运行过程中产生的静态数据，比如日志数据。

## 数据接入

日志数据数据采集。用户使用和网页，app等，在与客户端交互过程，比如浏览网页，点击，评论等，会产生一些日志数据，通过数据埋点，将数据从日志服务器采集到分布式存储系统。

- 采用高可用的分布式海量日志采集系统Flume，提供拦截器对数据进行ETL处理，支持在日志系统中定制各类数据发送方，用于收集数据。

业务数据接入。用户登录网站进行浏览，可以通过分类查询和全文检索寻找需要的产品，这些产品数据存在业务数据库中。

- 采用华为的数据接入服务（Data Ingestion Service，简称DIS）可让您轻松收集、处理和分发实时流数据，以便您对新信息快速做出响应。
- 采用datax工具实现大数据存储系统和关系型数据库之间进行数据交换。datax 是一个异构数据源同步工具，采用Framework + plugin架构构建，可以在关系型数据库、文件存储系统、Hive、HBase等各种异构数据源之间进行数据同步，支持流量控制和运行信息收集，跟踪数据同步情况。

## 消息队列

对于采集到的日志数据我们可以同步接入到消息队列，可以起到系统之间解耦，峰值缓冲和异步通信的作用，消息队列可以有支持横向扩展，吞吐量高，延时低的特点，为后续实时流式计算提供数据来源。

- 可以采用Kafka作为消息队列。kafka是一个高吞吐，低延时的分部式消息发布和订阅系统， Kafka是天生是分布式的，数据副本冗余、流量负载均衡、可扩展有较长时间持久化，顺序读和顺序写，通过零拷贝的技术消息直接持久化磁盘等特点。在大数据场景中广泛应用于实时数据计算和日志采集。

## 数据存储

批处理的数据一般存储在分布式文件存储系统中，该存储系统可以存放各种格式的文件。本文采用obs对象存储服务进行数据的存储。

- 对象存储服务（Object Storage Service，简称OBS），是一个基于对象的海量存储服务，为客户提供海量、安全、高可靠、低成本的数据存储能力。数据存储和计算分离，使用时无需考虑容量限制，并且提供多种存储类型供选择，满足客户各类业务场景诉求。OBS服务实现了hadoop的HDFS协议，在大数据场景中可以替代hadoop系统中的HDFS服务，实现Spark、MapReduce、Hive、HBase等大数据生态与OBS服务的对接，为大数据计算提供“数据湖”存储。

  

## 全流程调度

本分析运营平台由多个任务组成，各个任务单元有前后依赖关系。需要一个批量工作流任务调度器，在一个工作流内以一个特定的顺序运行一组工作和流程

- 使用任务流调度工具azkaban，每天凌晨进行跑批操作，将shell脚本,hive脚本,java程序进行全流程调度执行。Executor Server处理工作流和作业的实际执行，Web Server处理项目管理，身份验证，计划和执行触发。



![](img/azkbanschedule.png)



## BI工具可视化

大多数大数据处理的目的都是通过分析和报告，把运营数据转换成战略决策信息，对企业的发展历程和未来趋势做出定量分析和预测，最终是要给到公司管理决策层进行直观可视化展示的。

- BI可视化可以使用到华为的数据可视化服务（Data Lake Visualization，简称DLV）是一站式数据可视化平台，适配云上云下多种数据源，提供各种可视化组件，采用拖拽式自由布局，快速定制和应用属于您自己的数据大屏。



### **可视化展示方案**：

本项目有两种可视化展示方案，方案一为通过obs对象存储系统直接对接可视化工具DLV，方案二为通过MySQL数据库对接可视化工具DLV。其中OBS系统中将查询语句以csv文件进行存储，而MySQL数据库则需要先行创建好表。

[DLV 参考：]: https://support.huaweicloud.com/intl/zh-cn/dlv/index.html



#### **方案一**

1.将文件下载到本地，以.csv文件进行保存

执行语句：

```
###hive -e "set hive.cli.print.header=true;select * from ods_t_order" | sed 's/[\t]/,/g' > /home/huaweicloud/tmp_t_order.csv


```

2.将本地.csv文件上传至obs中

执行语句：

```
hadoop fs -put /home/huaweicloud/tmp_t_order.csv obs://obs-east-bkt002/warehouse/hive/ads/tmp_t_order1.csv
```

3.在dlv数据可视化界面中实现可视化展示



#### **方案二**

将obs数据导入到MYSQL，此处以”统计近60天各租户下单省份地图“为例

1.创建HIVE表

```
DROP TABLE IF EXISTS ads_days_province;
CREATE EXTERNAL TABLE ads_days_province(
domain String,
province String,
amount bigint,
) COMMENT 'ads_days_province'
ROW FORMAT DELIMITED FIELDS TERMINATED BY '\t'
LOCATION 'obs://obs-east-bkt002/warehouse/hive/ads/ads_days_province/';
```

2.将数据填入到HIVE表

``` 
inseret overwrite table ads_days_province SELECT *from ads_days_province
```

3.将HIVE表obs数据导入到云数据库MYSQL中

```
 python /opt/module/datax/bin/datax.py  /opt/module/datax/job/ads_province.json
```

4.在DLV中创建连接，并进行可视化展示



**sql查询指标展示**

1.统计近60天各租户的营业额

 ads_days_amount

```
hive - e "set hive.cli.print.header=true;
SELECT
domain,
sum( payment ) 
FROM
ods_t_order 
WHERE
to_date ( CURRENT_DATE )<= date_add( to_date ( created_time ), 60 ) 
GROUP BY
domain " | sed 's/[\t]/,/g' > /home/huaweicloud/ads_days_amount.csv ;
hadoop fs -put /home/huaweicloud/ads_days_amount.csv obs://obs-east-bkt002/warehouse/hive/ads/ads_days_amount.csv
```

​	字段定义：

 current_date  系统当前时间

 created_time 订单表中订单的创建时间

 domain 租户的域名

 payment 每个订单的实际付款

![](img/ads_days_amount.PNG)

 

2.统计近60天各租户下单省份地图

```
SELECT DISTINCT
	c.domain,
	c.province,
	count() over (
	partition BY ( c.domain, c.province )) amount 
FROM
	(
	SELECT
		b.domain,
		province,
		count() over (
		PARTITION BY ( domain, province )) rank 
	FROM
		(
		SELECT
			domain,
			substr( address, 0, 2 ) province 
		FROM
			ods_t_order 
		WHERE
			address NOT LIKE "黑龙%" 
			AND address NOT LIKE "内蒙%" 
			AND address NOT LIKE "钓鱼%" 
			AND to_date ( CURRENT_DATE )<= date_add( to_date ( created_time ), 60 ) 
		) b UNION ALL
		(
		SELECT
			b.domain,
			province,
			count() over (
			PARTITION BY ( domain, province )) rank 
		FROM
			(
			SELECT
				domain,
				substr( address, 0, 3 ) province 
			FROM
				ods_t_order 
			WHERE
				address LIKE "黑龙%" 
				AND address LIKE "内蒙%" 
				AND address LIKE "钓鱼%" 
				AND to_date ( CURRENT_DATE )<= date_add( to_date ( created_time ), 60 ) 
			) b 
		) 
	)c
```

 字段定义：

 address 订单表中各订单的下单地址

 domain 各租户的域名

province 各订单下单省份

amount  各租户下的各省份订单数量

![](img/ads_days_province.PNG)



3.统计近30天各租户top5热销产品

```
SELECT
			* 
		FROM
			(
			SELECT
				a.domain,
				a.sku_id,
				a.amount,
				row_number() over ( PARTITION BY a.domain ORDER BY a.domain, a.amount DESC ) rank 
			FROM
				(
				SELECT DISTINCT
					domain,
					sku_id,
					count() over ( PARTITION BY domain, sku_id ) amount 
				FROM
					ods_t_order 
				WHERE
					to_date ( CURRENT_DATE )<= date_add( to_date ( created_time ), 30 ) 
				) a 
			) b 
		WHERE
			b.rank <= 5;
	
```

 字段定义：

domain  各租户的域名

sku_id  各服务下的商品id

amount 各租户订单下的各商品销售数量

rank  对各租户下根据商品销售数量进行排序

![](img/ads_days_sale.PNG)



4.统计出各租户30天内销售的sku_id 及数量

```
		SELECT DISTINCT
			domain,
			sku_id,
			count() over ( PARTITION BY domain, sku_id ) amount 
		FROM
			ods_t_order 
		WHERE
			to_date ( CURRENT_DATE )<= date_add( to_date ( created_time ), 30 ) 
		ORDER BY
			domain,
			amount DESC;
```

 domain 各租户的域名

sku_id  各服务下的商品id

amount 各租户订单下的各商品销售数量

 ![](img/ads_days_sku_id.PNG)



5.统计各租户近30天每日新增用户数

```
	SELECT DISTINCT
			a.domain,
			a.day,
			count() over (
			PARTITION BY ( a.domain, a.DAY )) num 
		FROM
			(
			SELECT
				domain,
				to_date ( created_time ) DAY 
			FROM
				ods_user 
			WHERE
				to_date ( CURRENT_DATE )<= date_add( to_date ( created_time ), 30 ) 
				AND account_type = 1 
			)a
```

domain  各租户的域名

created_time  各用户的创建时间

day  各用户账号的创建日期

num 各租户每天的用户创建数



![](img/ads_days_user.PNG)



6.统计各租户用户一周内所登录的时间段(一小时为一个时段)

```
 SELECT DISTINCT
			a.tenantDomain,
			HOUR,
			count() over (
			PARTITION BY ( a.tenantdomain, HOUR )) amount 
		FROM
			(
			SELECT
				tenantDomain,
				substr( timestr, 12, 2 ) HOUR,
				uid 
			FROM
				dwd_page_log 
			WHERE
				last_page_id = 'login' 
				AND page_id = 'home_service_list' 
				AND to_date ( CURRENT_DATE )<= date_add( to_date ( timestr ), 6 ) 
			) a
			LEFT JOIN ods_user ON a.uid = user_id 
			AND account_type = 1;
```

tenantDomain  各租户的域名

timestr  各用户的创建时间

hour 各用户的创建的时间段

page_id 当前浏览页面

last_page_id 上个浏览页面

account_type 各账号的种类

amount  各域名下每个小时的用户创建数



7.统计各租户近30天的用户日活量

```
SELECT DISTINCT
			tenantDomain,
			to_date ( timestr ) DAY,
			count() over (
			PARTITION BY ( tenantDomain, to_date(timestr) )) num 
		FROM
			dwd_page_log 
		WHERE
			last_page_id = 'login' 
			AND page_id = 'home_service-list' 
		AND to_date ( CURRENT_DATE )<= date_add( to_date ( timestr ), 30 )
```

tenantDomain  各租户的域名

day  各用户账号的创建日期

num 各租户下每天的用户创建数



8.统计近30天各租户的热门浏览页面top5（浏览次数最多的界面）

```
SELECT
			* 
		FROM
			(
			SELECT
				a.tenantDomain,
				a.page_id,
				a.amount,
				row_number() over ( PARTITION BY a.tenantDomain ORDER BY a.tenantDomain, a.amount DESC ) rank 
			FROM
				(
				SELECT DISTINCT
					tenantDomain,
					page_id,
					count() over ( PARTITION BY tenantDomain, page_id ) amount 
				FROM
					dwd_page_log 
				WHERE
					to_date ( CURRENT_DATE )<= date_add( to_date ( created_time ), 30 ) 
				) a 
			) b 
		WHERE
		b.rank <= 5;
```

tenantDomain  各租户的域名

page_id 当前浏览页面

amount  各租户下当前页面的浏览数

created_time 当前页面的访问时间

rank  各租户的浏览页面次数排序









